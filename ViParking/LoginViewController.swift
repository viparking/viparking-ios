//
//  LoginViewController.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/7/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class LoginViewController : UIViewController, WSDelegate {
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        btnLogin.layer.cornerRadius = 4
    }
    @IBAction func btnLoginClick(sender: AnyObject) {
        
        if validateInput() {
            let ws = ParkingWS(delegate: self)
            showLoadingPopUp()
            ws.login(txtUsername.text!, password: txtPassword.text!)
        }
    }
    func onSuccess(responseObject:WSResponse)
    {
        if let response = responseObject as? LoginResponse {
//            response.parse(response.jsonObject)
            if response.isMember {
                if let user = response.user {
                    if user.isBanned {
                        showErrorPopUp("ERROR_BANNED".localize())
                        return
                    }
                    Util.ACCESS_TOKEN = response.token
                    Util.USER_ID = user.id
                    Util.PHONE_NUMBER = user.phoneNumber
                    dismissPopUp({
                      self.performSegueWithIdentifier("DashboardSegue", sender: self)  
                    })
                }
            }
            else {
                showErrorPopUp("ERROR_NOT_MEMBER".localize())
            }
        }
        else{
            showErrorPopUp("SERVER_ERROR".localize())
        }
    }
    func onError(error:String, request: HTTPRequest)
    {
        showErrorPopUp(error)
    }
    func onNoNetwork(request: HTTPRequest)
    {
        showRetryLoading(request)
    }
    
    func validateInput() -> Bool
    {
        if txtUsername.text?.characters.count == 0 {
            txtUsername.shake(.Horizontal)
            return false
        }
        else if txtPassword.text?.characters.count == 0{
            txtPassword.shake(.Horizontal)
            return false
        }
        return true
    }
    @IBAction func dismissKeyboard(sender: AnyObject) {
        txtUsername.resignFirstResponder()
        txtPassword.resignFirstResponder()
    }
}