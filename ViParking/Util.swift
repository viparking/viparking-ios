//
//  Util.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation
public class Util{
    public static let NOT_LOGGED_IN = 401
    
    public static let SHOULD_LOGOUT = "odeo.shouldLogOut"
    public static var IS_ONBOARDING = false
    public static let API_KEY = "263b7f33fdfb189bb5bac3a6ba021637"
    public static let BASE_URL = "http://192.168.253.101/parkingws/public/API/"
    
    public static let ONBOARDING_DELAY = 5.0
    public static let WS_OFFSET = 1
    
    //MARK: - Key For NSUserDefault
    private static let KEY_TREE_FEATURE_SETTING = "treeSetting"
    private static let KEY_USER_EMAIL = "Odeo.UserEmail"
    private static let USER_ID_DEFAULT = "Odeo.UserID"
    private static let PHONE_NUMBER_DEFAULT = "Odeo.PhoneNumber"
    private static let TOKEN_DEFAULT = "Odeo.Token"
    private static let REFRESH_TOKEN_DEFAULT = "Odeo.RefreshToken"
    private static let VEHICLE_LIST_DEFAULT = "Parking.VehicleList"
    private static let SELCTED_VEHICLE_INDEX_DEFAULT = "Parking.vehicleIndex"
    
    public static let LOGIN_URL = BASE_URL + "user/login"
    public static let VEHICLE_LIST_URL = BASE_URL + "vehicle/me"
    public static let CHANGE_PASSWORD_URL = BASE_URL + "me/changePassword"
    public static let HISTORY_URL = BASE_URL + "vehicle/me/"
    public static let SEND_MAIL_URL = BASE_URL + "vehicle/me"
    
    public static func generateBearerToken() -> String?{
        if let userID = Util.USER_ID, accessToken = Util.ACCESS_TOKEN
        {
            let rawString = accessToken + ":" + String(userID) + ":" + API_KEY;
            if let data = (rawString as
                NSString).dataUsingEncoding(NSUTF8StringEncoding)
            {
                return data.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
            }
            return nil
        }
        return nil
    }
    
    public static var USER_ID:String?{
        get{
        return NSUserDefaults.standardUserDefaults().objectForKey(USER_ID_DEFAULT) as? String
        }
        set{
            let userDefaults = NSUserDefaults.standardUserDefaults()
            if let userID = newValue
            {
                userDefaults.setObject(userID, forKey: USER_ID_DEFAULT)
                userDefaults.synchronize()
            }
        }
    }
    
    public static var ACCESS_TOKEN:String?{
        get{
        if let token = NSUserDefaults.standardUserDefaults().stringForKey(TOKEN_DEFAULT){
        return token
        }
        return nil;
        }
        set{
            let userDefaults = NSUserDefaults.standardUserDefaults()
            if let accessToken = newValue
            {
                userDefaults.setObject(accessToken, forKey: TOKEN_DEFAULT)
                userDefaults.synchronize()
            }
        }
    }
    
    public static var REFRESH_TOKEN:String?{
        get{
        if let token = NSUserDefaults.standardUserDefaults().valueForKey(REFRESH_TOKEN_DEFAULT) as? String{
        return token
        }
        return nil;
        }
        set{
            let userDefaults = NSUserDefaults.standardUserDefaults()
            if let refreshToken = newValue
            {
                userDefaults.setValue(refreshToken, forKey: REFRESH_TOKEN_DEFAULT)
                userDefaults.synchronize()
            }
        }
    }
    public static var TREE_FEATURE_SETTING:String{
        get{
        if let returnValue = NSUserDefaults.standardUserDefaults().objectForKey(KEY_TREE_FEATURE_SETTING) as? String{
        return returnValue
    }
        else{
        return "250"
        }
        }
        set{
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setObject(newValue, forKey: KEY_TREE_FEATURE_SETTING)
            userDefaults.synchronize()
        }
    }
    public static var USER_EMAIL:String?{
        get{
        if let returnValue = NSUserDefaults.standardUserDefaults().objectForKey(KEY_USER_EMAIL) as? String{
        return returnValue
    }
        else{
        return nil
        }
        }
        set{
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setObject(newValue, forKey: KEY_USER_EMAIL)
            userDefaults.synchronize()
        }
    }
    
    public static var PHONE_NUMBER:String?{
        get{
        if let returnValue = NSUserDefaults.standardUserDefaults().objectForKey(PHONE_NUMBER_DEFAULT) as? String {
        return returnValue;
    } else {
        return nil
        }
        }
        set{
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setObject(newValue, forKey: PHONE_NUMBER_DEFAULT)
            userDefaults.synchronize()
            
        }
    }
    
    static var VEHICLE_LIST:[Vehicle]?{
        get{
            if let unarchivedObject = NSUserDefaults.standardUserDefaults().objectForKey(VEHICLE_LIST_DEFAULT) as? NSData {
                return NSKeyedUnarchiver.unarchiveObjectWithData(unarchivedObject) as? [Vehicle]
            }
            else {
                return nil
            }
        }
        set{
            let userDefaults = NSUserDefaults.standardUserDefaults()
            if let value = newValue {
                let archivedObject = NSKeyedArchiver.archivedDataWithRootObject(value as NSArray)
                userDefaults.setObject(archivedObject, forKey: VEHICLE_LIST_DEFAULT)
                userDefaults.synchronize()
            }
            else {
                userDefaults.setObject(newValue, forKey: VEHICLE_LIST_DEFAULT)
                userDefaults.synchronize()
            }
            
        }
    }
    
    public static var SELECTED_VEHICLE_INDEX:Int?{
        get{
        if let returnValue = NSUserDefaults.standardUserDefaults().objectForKey(SELCTED_VEHICLE_INDEX_DEFAULT) as? Int {
        return returnValue;
    } else {
        return nil
        }
        }
        set{
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setObject(newValue, forKey: SELCTED_VEHICLE_INDEX_DEFAULT)
            userDefaults.synchronize()
            
        }
    }
    
    public static func delay(delay:Double, closure:()->()){
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    public static func makeMoneyFormat(var input:Int) -> String
    {
        if input == 0{
            return "Rp0"
        }
        var isNegative = false
        if input<0 {
            input *= -1
            isNegative = true
        }
        var output:String = ""
        var counter:Int = 0
        while input >= 1 {
            counter += 1
            output = String(input % 10) + output
            if (counter % 3) == 0 && input > 10 {
                output = "," + output
            }
            input /= 10
        }
        if isNegative {
            return "- Rp"+output
        }
        else {
            return "Rp"+output
        }
    }
    
    public enum insetHeightForKeyboard:Int {
        case Normal,
        HasErrorLabel = 16,
        HasBarItem = 32,
        HasErrorNBar = 48
    }
    
}