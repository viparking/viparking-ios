//
//  SettingViewController.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/7/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class SettingViewController : UIViewController, WSDelegate{
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtNewPasswordConfirm: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnOkClick(sender: AnyObject) {
        if validateInput() {
            let ws = ParkingWS(delegate: self)
            showLoadingPopUp()
            ws.changePassword(txtOldPassword.text!, newPassword: txtNewPassword.text!)
        }
    }
    
    func validateInput() -> Bool {
        if txtOldPassword.text?.characters.count == 0 {
            txtOldPassword.shake(.Horizontal)
            return false
        }
        else if txtNewPasswordConfirm.text?.characters.count == 0{
            txtNewPasswordConfirm.shake(.Horizontal)
            return false
        }
        else if txtNewPassword.text?.characters.count == 0 {
            txtNewPassword.shake(.Horizontal)
            return false
        }
        else if txtNewPassword.text != txtNewPasswordConfirm.text {
            txtNewPassword.shake(.Horizontal)
            txtNewPasswordConfirm.shake(.Horizontal)
            return false
        }

        return true
    }
    
    @IBAction func btnRemoveResponder(sender: AnyObject) {
        txtOldPassword.resignFirstResponder()
        txtNewPassword.resignFirstResponder()
        txtNewPasswordConfirm.resignFirstResponder()
    }
    
    @IBAction func btnLogoutClick(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
        Util.USER_EMAIL = ""
        Util.USER_ID = ""
        Util.ACCESS_TOKEN = ""
    }
    
    func onSuccess(responseObject:WSResponse) {
        dismissPopUp({
            let msgPopUp = UIAlertController(title: "SUCCESS".localize(), message: "PASSWORD_CHANGED".localize(), preferredStyle: .Alert)
            
            msgPopUp.addAction(UIAlertAction(title: "OK".localize(), style: .Default, handler: nil))
            
            dispatch_async(dispatch_get_main_queue(), {
                self.presentViewController(msgPopUp, animated: true, completion: nil)
            })
            self.txtNewPassword.text = ""
            self.txtOldPassword.text = ""
            self.txtNewPasswordConfirm.text = ""
        })
    }
    
    func onError(error:String, request: HTTPRequest)
    {
        if error == "" {
            dismissPopUp({
                self.dismissViewControllerAnimated(true, completion: nil)
                Util.USER_EMAIL = ""
                Util.USER_ID = ""
                Util.ACCESS_TOKEN = ""
            })
            return
        }
        showErrorPopUp(error)
    }
    
    func onNoNetwork(request: HTTPRequest)
    {
        showRetryLoading(request)
    }
}