//
//  User.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

public class User {
    var id:String? = nil
    var memberTypeID:Int? = nil
    var username:String? = nil
    var email:String? = nil
    var phoneNumber:String? = nil
    var point:Int? = nil
    var isBanned:Bool = false
    var alamatKTP:String? = nil
    var alamatTinggal:String? = nil
    var noKTP:String? = nil
    var memberType:String? = nil
    
    init(jsonObject:[String:AnyObject]){
        id = jsonObject["id"] as? String
        memberTypeID = jsonObject["member_type_id"] as? Int
        username = jsonObject["username"] as? String
        email = jsonObject["email"] as? String
        phoneNumber = jsonObject["phoneNumber"] as? String
        point = jsonObject["point"] as? Int
        isBanned = jsonObject["isBanned"] as! Int == 1
        alamatKTP = jsonObject["alamatKTP"] as? String
        alamatTinggal = jsonObject["alamatTinggal"] as? String
        noKTP = jsonObject["noKTP"] as? String
        memberType = jsonObject["memberType"] as? String
    }
}