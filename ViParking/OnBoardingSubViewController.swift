//
//  OnBoardingSubViewController.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/8/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class OnBoardingSubViewController : UIViewController {
    var pageIndex : Int = 0
    var titleText : String = ""
    var imageFile : String = ""
    var descrText : String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let label = UILabel(frame: CGRectMake(20, view.frame.height*3/4 - 46, view.frame.width-40, 20))
        label.font = UIFont (name: "AvenirNext-Medium", size: 16)
        
        label.textColor = UIColor.whiteColor()
        label.text = titleText
        label.textAlignment = .Center
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        view.addSubview(label)
        
        let descr = UILabel(frame: CGRectMake(20, view.frame.height*3/4 - 30, view.frame.width-40, 32))
        descr.textColor = UIColor.whiteColor()
        descr.font = UIFont (name: "AvenirNext-Regular", size: 14)
        descr.text = descrText
        descr.textAlignment = .Center
        descr.lineBreakMode = NSLineBreakMode.ByWordWrapping
        descr.numberOfLines = 3
        view.addSubview(descr)
        
        
        let image = UIImage(named: imageFile)
        let imageView = UIImageView(image: image!)
        imageView.frame = CGRect(x: 16, y: 80 , width: self.view.frame.width-32, height: self.view.frame.width-32)
        imageView.contentMode = .ScaleAspectFit
        
//        let shadowView = UIView(frame: CGRect(x: 0, y: 0, width: imageView.bounds.size.width, height: imageView.bounds.size.height))
//        shadowView.layer.cornerRadius = 10
//        shadowView.backgroundColor = UIColor.clearColor()
//        
//        let shadow = CAGradientLayer()
//        shadow.startPoint = CGPoint(x: 0.5, y: 1.0)
//        shadow.endPoint = CGPoint(x: 0.5, y: 0.0)
//        shadow.frame = CGRect(x: 0, y: imageView.bounds.size.height - 6, width: imageView.bounds.size.width, height: 6)
//        shadow.colors = [UIColor(hexaString: "#000000", alphaValue: 0.2).CGColor, UIColor.clearColor().CGColor]
//        shadowView.layer.insertSublayer(shadow, atIndex:0)
//        
//        imageView.addSubview(shadowView)
//        
//        imageView.bringSubviewToFront(shadowView)
        view.addSubview(imageView)
//        view.bringSubviewToFront(shadowView)
        view.sendSubviewToBack(imageView)
        
        
        //        getImageWithColor(UIColor(hexaString: "10393F", alphaValue: 1), size: CGSizeMake(self.view.frame.width, 56))
        //
        //    let button = UIButton(type: UIButtonType.System)
        //    button.frame = CGRectMake(20, view.frame.height - 160, view.frame.width - 160, 50)
        //    button.backgroundColor = UIColor(red: 138/255.0, green: 181/255.0, blue: 91/255.0, alpha: 1)
        //    button.setTitle(titleText, forState: UIControlState.Normal)
        //    button.addTarget(self, action: "Action:", forControlEvents: UIControlEvents.TouchUpInside)
        //    self.view.addSubview(button)
    }
    
    func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    override func viewDidAppear(animated: Bool) {
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
