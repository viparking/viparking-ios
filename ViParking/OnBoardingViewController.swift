//
//  OnBoardingViewController.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/7/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class OnBoardingViewController : UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pageViewController : UIPageViewController?
    var pageTitles : Array<String> = ["ONBOARDING_TITLE_1".localize(), "ONBOARDING_TITLE_2".localize(), "ONBOARDING_TITLE_3".localize() , "ONBOARDING_TITLE_4".localize()]
    var pageDescr : Array<String> = ["ONBOARDING_CONTENT_1".localize()
        , "ONBOARDING_CONTENT_2".localize()
        , "ONBOARDING_CONTENT_3".localize()
        , "ONBOARDING_CONTENT_4".localize()]
    var pageImages : Array<String> = ["img_onboarding_2", "img_onboarding_2", "img_onboarding_3", "img_onboarding_4"]
    var nextIndex : Int = 0
    var pageControl : UIPageControl?
    var currentIndex : Int = 0
    
    @IBOutlet weak var btnSkip: UIButton!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        btnSkip.frame = CGRectMake(btnSkip.frame.origin.x, view.frame.size.height * 3 / 4 + 76, btnSkip.frame.size.width, btnSkip.frame.size.height)
        btnSkip.layer.borderWidth = 0.5
        btnSkip.layer.borderColor = UIColor(hexaString: "#FFFFFF", alphaValue: 0.5).CGColor
        
        if let userID = Util.USER_ID {
            if userID != "" {
                self.performSegueWithIdentifier("DashboardSegue", sender: self)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if pageControl == nil {
            pageControl = UIPageControl(frame: CGRectMake(0, view.frame.size.height * 3 / 4 + 10, view.frame.size.width, 50))
            pageControl!.numberOfPages = self.pageTitles.count
            pageControl!.transform = CGAffineTransformMakeScale(2.5, 2.5)
            view.addSubview(pageControl!.viewForBaselineLayout())
            
            pageViewController = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
            pageViewController!.dataSource = self
            pageViewController?.delegate = self
            
            let startingViewController: OnBoardingSubViewController = viewControllerAtIndex(0)!
            let viewControllers = [startingViewController]
            pageViewController!.setViewControllers(viewControllers , direction: .Forward, animated: false, completion: nil)
            pageViewController!.view.frame = CGRectMake(0, -30, view.frame.size.width, view.frame.size.height-10);
            
            addChildViewController(pageViewController!)
            view.addSubview(pageViewController!.view)
            view.bringSubviewToFront(btnSkip)
            autoScroll()
        }
    }
    
    func autoScroll() {
        Util.delay(Util.ONBOARDING_DELAY, closure: {
            if self.currentIndex == self.pageTitles.count-1 {
                self.currentIndex = 0
            }
            else {
                self.currentIndex += 1
            }
            let startingViewController: OnBoardingSubViewController = self.viewControllerAtIndex(self.currentIndex)!
            let viewControllers = [startingViewController]
            self.pageViewController!.setViewControllers(viewControllers , direction: .Forward, animated: true, completion: nil)
            self.pageControl!.currentPage = self.currentIndex
            self.autoScroll()
        })
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if Util.USER_ID != nil && Util.USER_ID != "" {
            self.performSegueWithIdentifier("DashboardSegue", sender: self)
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! OnBoardingSubViewController).pageIndex
        
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index--
        
        return viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! OnBoardingSubViewController).pageIndex
        
        if index == NSNotFound {
            return nil
        }
        index++
        if (index == self.pageTitles.count) {
            return nil
        }
        
        return viewControllerAtIndex(index)
    }
    
    func viewControllerAtIndex(index: Int) -> OnBoardingSubViewController?
    {
        if self.pageTitles.count == 0 || index >= self.pageTitles.count
        {
            return nil
        }
        // Create a new view controller and pass suitable data.
        let pageContentViewController = OnBoardingSubViewController()
        pageContentViewController.imageFile = pageImages[index]
        pageContentViewController.titleText = pageTitles[index]
        pageContentViewController.descrText = pageDescr[index]
        pageContentViewController.pageIndex = index
        return pageContentViewController
    }
    
    func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController]) {
        if let pendingViewController = pendingViewControllers[0] as? OnBoardingSubViewController {
            nextIndex = pendingViewController.pageIndex
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            pageControl!.currentPage = nextIndex
        }
    }
}