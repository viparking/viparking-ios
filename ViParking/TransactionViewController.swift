//
//  TransactionViewController.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/7/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class TransactionViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, WSDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var lblSaldo: UILabel!
    @IBOutlet weak var lblPlateNumber: UILabel!
    @IBOutlet weak var txtStartTime: UITextField!
    @IBOutlet weak var txtEndTime: UITextField!
    
    private var page:Int = 0
    private var isAppending = false
    private var vehicleID:String = ""
    private var histories:[History] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        historyTableView.delegate = self
        historyTableView.dataSource = self
        txtStartTime.delegate = self
        txtEndTime.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        isAppending = false
        page = 0
        
        let image = UIImage(named: "calendar-icon")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRectMake(0.0, 0.0, image!.size.width+10.0, image!.size.height);
        imageView.contentMode = UIViewContentMode.Center
        txtStartTime.leftViewMode = UITextFieldViewMode.Always
        txtStartTime.leftView = imageView
        
        let secondImage = UIImage(named: "calendar-icon")
        let secondImageView = UIImageView(image: secondImage)
        secondImageView.frame = CGRectMake(0.0, 0.0, secondImage!.size.width+10.0, secondImage!.size.height);
        secondImageView.contentMode = UIViewContentMode.Center
        txtEndTime.leftViewMode = UITextFieldViewMode.Always
        txtEndTime.leftView = secondImageView
        
        if let vehicleList = Util.VEHICLE_LIST {
            if vehicleList.count > 0 {
                let ws = ParkingWS(delegate: self)
                if let selectedIndex = Util.SELECTED_VEHICLE_INDEX {
                    vehicleID = vehicleList[selectedIndex].id!
                    self.lblPlateNumber.text = "SALDO".localize() + " " + vehicleList[selectedIndex].plateNumber!
                    self.lblSaldo.text = Util.makeMoneyFormat(vehicleList[selectedIndex].balance!)
                }
                else {
                    vehicleID = vehicleList[0].id!
                    self.lblPlateNumber.text = "SALDO".localize() + " " + vehicleList[0].plateNumber!
                    self.lblSaldo.text = Util.makeMoneyFormat(vehicleList[0].balance!)
                }
                showLoadingPopUp()
                ws.getHistory(vehicleID, page: page, startTime: nil, endTime: nil)
            }
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        showDateInput(textField)
    }
    
    func showDateInput(textField: UITextField)
    {
        txtStartTime.resignFirstResponder()
        txtEndTime.resignFirstResponder()
        let errorPopUp = UIAlertController(title: "SELECT_DATE".localize(), message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: .ActionSheet)
        
        var frame = errorPopUp.view.bounds
        frame.origin.y += 20
        frame.size.height /= 3
        let datePicker = UIDatePicker(frame: frame)
        datePicker.datePickerMode = .Date
        
        errorPopUp.view.addSubview(datePicker)
        
        errorPopUp.addAction(UIAlertAction(title: "OK".localize(), style: .Default, handler: {action in
            dispatch_async(dispatch_get_main_queue(), {
                print(datePicker.date)
                let date = datePicker.date
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                textField.text = dateFormatter.stringFromDate(date)
                
                if self.txtEndTime.text?.characters.count > 0 && self.txtStartTime.text?.characters.count > 0 {
                    let ws = ParkingWS(delegate: self)
                    
                    let startTime = dateFormatter.dateFromString(self.txtStartTime.text!)
                    let endTime = dateFormatter.dateFromString(self.txtEndTime.text!)
                    
                    let invertedDateFormatter = NSDateFormatter()
                    invertedDateFormatter.dateFormat = "yyyy-MM-dd"
                    self.showLoadingPopUp()
                    ws.getHistory(self.vehicleID, page: self.page, startTime: invertedDateFormatter.stringFromDate(startTime!), endTime: invertedDateFormatter.stringFromDate(endTime!))
                }
            })}))
        errorPopUp.addAction(UIAlertAction(title: "CANCEL".localize(), style: .Cancel, handler: nil))
        
        self.presentViewController(errorPopUp, animated: true, completion: nil)
    }
    
    @IBAction func btnSendEmailClick(sender: AnyObject) {
        let ws = ParkingWS(delegate: self)
        showLoadingPopUp()
        
        if txtStartTime.text?.characters.count > 0 && txtEndTime.text?.characters.count > 0 {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            
            let startTime = dateFormatter.dateFromString(self.txtStartTime.text!)
            let endTime = dateFormatter.dateFromString(self.txtEndTime.text!)
            
            let invertedDateFormatter = NSDateFormatter()
            invertedDateFormatter.dateFormat = "yyyy-MM-dd"
            
            ws.sendEmail(self.vehicleID, startTime: invertedDateFormatter.stringFromDate(startTime!), endTime: invertedDateFormatter.stringFromDate(endTime!))
        }
        else {
            ws.sendEmail(self.vehicleID, startTime: nil, endTime: nil)
        }
    }
    
    @IBAction func btnDownloadPdfClick(sender: AnyObject) {
        self.performSegueWithIdentifier("pdfSegue", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "pdfSegue" {
            if let nextViewController = segue.destinationViewController as? PDFViewController {
                
                if txtStartTime.text?.characters.count > 0 && txtEndTime.text?.characters.count > 0 {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    
                    let startTime = dateFormatter.dateFromString(self.txtStartTime.text!)
                    let endTime = dateFormatter.dateFromString(self.txtEndTime.text!)
                    
                    let invertedDateFormatter = NSDateFormatter()
                    invertedDateFormatter.dateFormat = "yyyy-MM-dd"
                    
                    nextViewController.startDate = invertedDateFormatter.stringFromDate(startTime!)
                    nextViewController.endDate = invertedDateFormatter.stringFromDate(endTime!)
                }
                else {
                    nextViewController.startDate = nil
                    nextViewController.endDate = nil
                }
                nextViewController.vehicleID = self.vehicleID
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return histories.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("historyCell") as! HistoryTableViewCell
        cell.setupView(histories[indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 145
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        let row = indexPath.row + 1
        if row == Util.WS_OFFSET * (page + 1)
        {
            isAppending = true
            //append
            let ws = ParkingWS(delegate: self)
            page += 1
            if txtStartTime.text?.characters.count > 0 && txtEndTime.text?.characters.count > 0 {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                
                let startTime = dateFormatter.dateFromString(self.txtStartTime.text!)
                let endTime = dateFormatter.dateFromString(self.txtEndTime.text!)
                
                let invertedDateFormatter = NSDateFormatter()
                invertedDateFormatter.dateFormat = "yyyy-MM-dd"
                
                ws.getHistory(self.vehicleID, page: self.page, startTime: invertedDateFormatter.stringFromDate(startTime!), endTime: invertedDateFormatter.stringFromDate(endTime!))
            }
            else {
                ws.getHistory(vehicleID, page: page, startTime: nil, endTime: nil)
            }

        }
    }
    
    func onSuccess(responseObject:WSResponse) {
        if isAppending {
            if let response = responseObject as? HistoryResponse {
                let historyList = response.histories
                for history in historyList {
                    self.histories.append(history)
                }
                Util.delay(0.1, closure: {
                    self.historyTableView.reloadData()
                })
            }
            return
        }
        if let response = responseObject as? HistoryResponse {
            dismissPopUp({
                self.histories = response.histories
                self.historyTableView.reloadData()
            })
        } else {
            dismissPopUp({
                let msgPopUp = UIAlertController(title: "SUCCESS".localize(), message: "EMAIL_SENT".localize(), preferredStyle: .Alert)
                
                msgPopUp.addAction(UIAlertAction(title: "OK".localize(), style: .Default, handler: nil))
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.presentViewController(msgPopUp, animated: true, completion: nil)
                })
            })
        }
    }
    
    func onError(error:String, request: HTTPRequest)
    {
        if error == "" {
            dismissPopUp({
                self.dismissViewControllerAnimated(true, completion: nil)
                Util.USER_EMAIL = ""
                Util.USER_ID = ""
                Util.ACCESS_TOKEN = ""
            })
            return
        }
        showErrorPopUp(error)
    }
    
    func onNoNetwork(request: HTTPRequest)
    {
        showRetryLoading(request)
    }
}