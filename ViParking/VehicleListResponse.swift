//
//  VehicleListResponse.swift
//  ViParking
//
//  Created by Ferico Samuel on 8/3/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

class VehicleListResponse : WSResponse {
    var vehicles:[Vehicle] = []
    
    override func parse(data: NSData) {
        super.parse(data)
        
        if let vehicleObjects = jsonObject["data"] as? [[String:AnyObject]] {
            for vehicle in vehicleObjects {
                let v = Vehicle(jsonObject: vehicle)
                vehicles.append(v)
            }
        }
    }
}