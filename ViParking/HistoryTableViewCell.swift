
//
//  HistoryTableViewCell.swift
//  ViParking
//
//  Created by Ferico Samuel on 8/6/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class HistoryTableViewCell : UITableViewCell
{
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    func setupView(history:History) {
        lblLocation.text = history.parkingLotName
        lblPrice.text = Util.makeMoneyFormat(history.price!)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let inTime = dateFormatter.dateFromString(history.inTime!)
        let outTime = dateFormatter.dateFromString(history.outTime!)
        
        let prefixFormatter = NSDateFormatter()
        prefixFormatter.dateFormat = "EEEE, dd MMMM yyyy, HH:mm"
        var prefix = prefixFormatter.stringFromDate(inTime!)
        
        let postfixFormatter = NSDateFormatter()
        postfixFormatter.dateFormat = "HH:mm"
        prefix += " - " + postfixFormatter.stringFromDate(outTime!)
        lblTime.text = prefix
        
        let hour = NSCalendar.currentCalendar().components(.Hour, fromDate: inTime!, toDate: outTime!, options: []).hour
        let minute = NSCalendar.currentCalendar().components(.Minute, fromDate: inTime!, toDate: outTime!, options: []).minute
        var duration = String(hour) + " " + "JAM".localize()
        duration += " " + String(minute) + " " + "MENIT".localize()
        lblDuration.text = duration
    }
}