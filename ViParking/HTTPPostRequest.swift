//
//  HTTPPostRequest.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

public class HTTPPostRequest:HTTPRequest
{
    init(isUsingHeader:Bool, url:NSURL, data:String, responseObject:WSResponse, delegate:WSDelegate)
    {
        super.init(delegate: delegate, url: url, responseObject: responseObject)
        self.request.HTTPMethod = "POST";
        let postString = data
        self.request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        makeRequest(isUsingHeader)
    }
    
    init(isUsingHeader:Bool, url:NSURL, data: NSData, responseObject:WSResponse, delegate:WSDelegate)
    {
        super.init(delegate: delegate, url: url, responseObject: responseObject)
        self.request.HTTPMethod = "POST";
        self.request.HTTPBody = data
        makeRequest(isUsingHeader)
    }
    init(isUsingHeader:Bool, url:NSURL, responseObject:WSResponse, delegate:WSDelegate)
    {
        super.init(delegate: delegate, url: url, responseObject: responseObject)
        self.request.HTTPMethod = "POST";
        makeRequest(isUsingHeader)
    }
}