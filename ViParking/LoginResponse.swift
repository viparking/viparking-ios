//
//  LoginResponse.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

public class LoginResponse : WSResponse
{
    var user:User? = nil
    var token:String? = nil
    var isMember:Bool = false
    
    override func parse(data: NSData) {
        super.parse(data)
        
        token = jsonObject["token"] as? String
        if let dataObject = jsonObject["data"] as? [String:AnyObject] {
            if let userObject = dataObject["user"] as? [String:AnyObject] {
                if let _ = userObject["memberType"] as? String {
                    self.user = User(jsonObject: userObject)
                    isMember = true
                }
                else {
                    isMember = false
                }
            }
        }
    }
}