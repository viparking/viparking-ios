//
//  UIColorExtension.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/8/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hexaString:String, alphaValue:Float) {
        self.init(
            red:   CGFloat( strtoul( String(Array(hexaString.characters)[1...2]), nil, 16) ) / 255.0,
            green: CGFloat( strtoul( String(Array(hexaString.characters)[3...4]), nil, 16) ) / 255.0,
            blue:  CGFloat( strtoul( String(Array(hexaString.characters)[5...6]), nil, 16) ) / 255.0, alpha: CGFloat(alphaValue) )
    }
}