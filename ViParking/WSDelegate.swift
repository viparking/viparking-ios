//
//  WSDelegate.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

public protocol WSDelegate:NSObjectProtocol
{
    func onSuccess(responseObject:WSResponse)
    func onError(error:String, request: HTTPRequest)
    func onNoNetwork(request: HTTPRequest)
}