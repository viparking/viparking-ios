//
//  VehicleListViewController.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/7/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class VehicleListViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, WSDelegate{
    
    
    @IBOutlet weak var vehicleTableView: UITableView!
    var vehicleList:[Vehicle] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        vehicleTableView.delegate = self
        vehicleTableView.dataSource = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let vehicles = Util.VEHICLE_LIST{
            vehicleList = vehicles
            vehicleTableView.reloadData()
        }
        else {
            let ws = ParkingWS(delegate: self)
            showLoadingPopUp()
            ws.getVehicleList()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let row = indexPath.row
        Util.SELECTED_VEHICLE_INDEX = row
        
        UIView.transitionFromView((DashboardTabBarController.instance?.selectedViewController!.view)!, toView: (DashboardTabBarController.instance?.viewControllers![0].view)!, duration: 0.5, options: UIViewAnimationOptions.TransitionFlipFromLeft, completion: {
            (finished:Bool) -> () in
                DashboardTabBarController.instance?.selectedIndex = 0
        })
        //        if let tabController = self.navigationController.
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 100
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicleList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("vehicleCell") as! VehicleTableViewCell
        let row = indexPath.row
        cell.setupView(vehicleList[row])
        return cell
    }
    
    func onSuccess(responseObject:WSResponse){
        dismissPopUp({
            if let response = responseObject as? VehicleListResponse {
                self.vehicleList = response.vehicles
                Util.VEHICLE_LIST = self.vehicleList
                self.vehicleTableView.reloadData()
            }
        })
    }
    
    func onError(error:String, request: HTTPRequest)
    {
        if error == "" {
            dismissPopUp({
                self.dismissViewControllerAnimated(true, completion: nil)
                Util.USER_EMAIL = ""
                Util.USER_ID = ""
                Util.ACCESS_TOKEN = ""
            })
            return
        }
        showErrorPopUp(error)
    }
    func onNoNetwork(request: HTTPRequest)
    {
        showRetryLoading(request)
    }
//
//    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
//        if let cell = cell as? VehicleTableViewCell {
//            cell.setupView()
//        }
//    }
//    
//    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
//        if let cell = cell as? VehicleTableViewCell {
//            cell.setupView()
//        }
//    }
}