//
//  NSMutableURLRequestExtension.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

extension NSMutableURLRequest{
    func useHeader(inout request:NSMutableURLRequest) -> Bool
    {
        if let bearer = Util.generateBearerToken()
        {
            request.addValue("Bearer "+bearer, forHTTPHeaderField: "Authorization")
            print(request.valueForHTTPHeaderField("Authorization"))
            return true
        }
        print("invalid header")
        return false
    }
}