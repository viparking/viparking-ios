//
//  ParkingWS.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

class ParkingWS:NSObject, NSURLSessionTaskDelegate
{
    var delegate:WSDelegate
    var request:HTTPRequest?
    
    /// Initialize Web service helper class
    /// - parameters:
    ///     - OdeoWSDelegate: class that implement this delegate which will be called on success or error during request
    init(delegate:WSDelegate)
    {
        self.delegate = delegate;
    }
    
    func retry(){
        if let request = self.request{
            request.retry()
        }
    }
    
    //MARK: USER Functionality
    func login(username:String, password:String)
    {
        print("logging in")
        if let url = NSURL(string: Util.LOGIN_URL)
        {
            print(url.absoluteString)
            let jsonObject: [String: AnyObject] = [
                "apiKey": Util.API_KEY,
                "data": [
                    "username":username,
                    "password":password
                ]
            ]
            do{
                let data =  try NSJSONSerialization.dataWithJSONObject(jsonObject, options: NSJSONWritingOptions.init(rawValue: 0))
                request = HTTPPostRequest(isUsingHeader: false, url: url, data: data, responseObject: LoginResponse(), delegate: self.delegate)
            }
            catch{
                print("Json serialization error")
            }
        }
    }
    
    func getVehicleList()
    {
        if let url = NSURL(string: Util.VEHICLE_LIST_URL) {
            print(url.absoluteString)
            request = HTTPGetRequest(isUsingHeader: true, url: url, responseObject: VehicleListResponse(), delegate: self.delegate)
        }
    }
    
    func changePassword(oldPassword: String, newPassword: String)
    {
        if let url = NSURL(string: Util.CHANGE_PASSWORD_URL) {
            let jsonObject: [String: AnyObject] = [
                "data": [
                    "oldPassword":oldPassword,
                    "newPassword":newPassword
                ]
            ]
            do{
                let data =  try NSJSONSerialization.dataWithJSONObject(jsonObject, options: NSJSONWritingOptions.init(rawValue: 0))
                request = HTTPPostRequest(isUsingHeader: true, url: url, data: data, responseObject: WSResponse(), delegate: self.delegate)
            }
            catch{
                print("Json serialization error")
            }

        }
    }
    
    func getHistory(vehicleID:String, page:Int, startTime:String?, endTime:String?) {
        var urlString = ""
        if let startTime = startTime, endTime = endTime {
            urlString = Util.HISTORY_URL + vehicleID + "/parking/"
            urlString += String(page) + "/" + startTime + "/" + endTime
        } else {
            urlString = Util.HISTORY_URL + vehicleID + "/parking/" + String(page)
        }
        
        urlString = urlString.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        if let url = NSURL(string: urlString) {
            print(url.absoluteString)
            request = HTTPGetRequest(isUsingHeader: true, url: url, responseObject: HistoryResponse(), delegate: self.delegate)
        }
    }
    
    func sendEmail(vehicleID:String, startTime:String?, endTime:String?) {
        var urlString = ""
        if let startTime = startTime, endTime = endTime {
            urlString = Util.SEND_MAIL_URL + vehicleID + "/email/"
            urlString += startTime + "/" + endTime
        } else {
            urlString = Util.HISTORY_URL + vehicleID + "/email/"
        }
        
        urlString = urlString.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        if let url = NSURL(string: urlString) {
            print(url.absoluteString)
            request = HTTPGetRequest(isUsingHeader: true, url: url, responseObject: WSResponse(), delegate: self.delegate)
        }
    }
}
