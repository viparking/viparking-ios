//
//  HTTPGetRequest.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

public class HTTPGetRequest: HTTPRequest
{
    init(isUsingHeader:Bool, url:NSURL, responseObject: WSResponse, delegate:WSDelegate)
    {
        super.init(delegate: delegate, url: url, responseObject: responseObject)
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET";
        makeRequest(isUsingHeader)
    }
}