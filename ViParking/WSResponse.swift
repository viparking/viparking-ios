//
//  WSResponse.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

public class WSResponse
{
    var isSuccess = false
    var jsonObject:[String:AnyObject] = [:]
    var errorMessage:String? = nil
    
    func parse(data:NSData)
    {
        do{
            jsonObject = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as! [String:AnyObject]
            isSuccess = jsonObject["success"] as! Bool
            if(!isSuccess){
                errorMessage = jsonObject["error"] as? String
            }
        }
        catch{
            print("error serializing JSON: \(error)")
        }
    }
}