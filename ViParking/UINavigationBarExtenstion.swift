//
//  UINavigationBarExtenstion.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func hideBottomHairline() {
        let navigationBarImageView = hairlineImageViewInNavigationBar(self)
        navigationBarImageView!.hidden = true
    }
    /*
    //Another way to hide
    for parent in self.navigationController!.navigationBar.subviews {
    for childView in parent.subviews {
    if(childView is UIImageView) {
    childView.removeFromSuperview()
    }
    }
    }
    */
    
    func showBottomHairline() {
        let navigationBarImageView = hairlineImageViewInNavigationBar(self)
        navigationBarImageView!.hidden = false
    }
    
    private func hairlineImageViewInNavigationBar(view: UIView) -> UIImageView? {
        if view.isKindOfClass(UIImageView) && view.bounds.height <= 1.0 {
            return (view as! UIImageView)
        }
        
        let subviews = (view.subviews as [UIView])
        for subview: UIView in subviews {
            if let imageView: UIImageView = hairlineImageViewInNavigationBar(subview) {
                return imageView
            }
        }
        
        return nil
    }
    
}