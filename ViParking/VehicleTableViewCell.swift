//
//  VehicleTableViewCell.swift
//  ViParking
//
//  Created by Ferico Samuel on 8/3/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class VehicleTableViewCell : UITableViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSub: UILabel!
    
    func setupView(vehicle:Vehicle) {
        outerView.layer.borderWidth = 1
        outerView.layer.borderColor = UIColor(hexaString: "#FFFFFF", alphaValue: 0.3).CGColor
        
        lblTitle.text = vehicle.plateNumber
        lblSub.text = vehicle.descriptions
    }
    
}