//
//  UIViewControllerExtension.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/8/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

private var scrollViewKey : UInt8 = 0
extension UIViewController{
    func showLoadingPopUp(){
        let loadingPopUp = UIAlertController(title: "LOADING".localize(), message: "\n\n", preferredStyle: .Alert)
        var frame = loadingPopUp.view.bounds
        frame.origin.y += 20
        let indicator = UIActivityIndicatorView(frame: frame)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        indicator.startAnimating()
        
        loadingPopUp.view.addSubview(indicator)
        dispatch_async(dispatch_get_main_queue(), {
            self.presentViewController(loadingPopUp, animated: true, completion: nil)
        })
    }
    
    func showRetryLoading(request:HTTPRequest){
        let retryPopUp = UIAlertController(title: "ERROR".localize(), message: "NO_NETWORK".localize(), preferredStyle: .Alert)
        
        retryPopUp.addAction(UIAlertAction(title: "RETRY".localize(), style: UIAlertActionStyle.Default, handler:{action in
            request.retry()
            self.showLoadingPopUp()
        }))
        
        retryPopUp.addAction(UIAlertAction(title: "CANCEL".localize(), style: .Cancel, handler: nil))
        
        dispatch_async(dispatch_get_main_queue(), {
            self.dismissViewControllerAnimated(false, completion: {
                self.presentViewController(retryPopUp, animated: true, completion: nil)
            })
        })
    }
    
    func showErrorPopUp(error:String){
        let errorPopUp = UIAlertController(title: "ERROR".localize(), message: error, preferredStyle: .Alert)
        
        errorPopUp.addAction(UIAlertAction(title: "OK".localize(), style: .Default, handler: nil))
        
        dispatch_async(dispatch_get_main_queue(), {
            self.dismissViewControllerAnimated(false, completion: {
                self.presentViewController(errorPopUp, animated: true, completion: nil)
            })
        })
    }
    
    func dismissPopUp(closure: () -> Void)
    {
        dispatch_async(dispatch_get_main_queue(), {
            self.dismissViewControllerAnimated(false, completion: closure)
        })
    }
    
    func dismissPopUp(){
        dispatch_async(dispatch_get_main_queue(), {
            self.dismissViewControllerAnimated(false, completion: nil)
        })
    }
    
    public func setupKeyboardNotifcationListenerForScrollView(scrollView: UIScrollView , insetType: Util.insetHeightForKeyboard = Util.insetHeightForKeyboard.Normal) {
        
        switch insetType{
        case .Normal:
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        case .HasErrorLabel:
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardHasErrorLabelWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
            
        case .HasBarItem:
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardHasBarButtonWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        case .HasErrorNBar:
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardHasErrorLabelAndBarButtonWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        default:
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        }
        
        
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
        internalScrollView = scrollView
    }
    
    public func removeKeyboardNotificationListeners() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private var internalScrollView: UIScrollView! {
        get {
            return objc_getAssociatedObject(self, &scrollViewKey) as? UIScrollView
        }
        set(newValue) {
            objc_setAssociatedObject(self, &scrollViewKey, newValue, .OBJC_ASSOCIATION_ASSIGN)
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        //        let dict = notification.object as! NSDictionary
        //        let additionForInset = dict["insetHeightScrollView"] as! CGFloat
        let userInfo = notification.userInfo as! Dictionary<String, AnyObject>
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey]!.intValue
        let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue
        let keyboardFrameConvertedToViewFrame = view.convertRect(keyboardFrame!, fromView: nil)
        let curveAnimationOption = UIViewAnimationOptions(rawValue: UInt(animationCurve))
        let options = UIViewAnimationOptions.BeginFromCurrentState
        UIView.animateWithDuration(animationDuration, delay: 0, options:options, animations: { () -> Void in
            //            let heightInternalSV = self.internalScrollView.frame.height
            var heightInternalSV = UIScreen.mainScreen().bounds.size.height
            if let _  = self.navigationController?.navigationBar{
                heightInternalSV -= 64.0
            }
            
            let insetHeight = (heightInternalSV + self.internalScrollView.frame.origin.y) - keyboardFrameConvertedToViewFrame.origin.y + 8
            self.internalScrollView.contentInset = UIEdgeInsetsMake(0, 0, insetHeight, 0)
            self.internalScrollView.scrollIndicatorInsets  = UIEdgeInsetsMake(0, 0, insetHeight, 0)
            }) { (complete) -> Void in
        }
    }
    
    
    func keyboardHasErrorLabelWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo as! Dictionary<String, AnyObject>
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey]!.intValue
        let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue
        let keyboardFrameConvertedToViewFrame = view.convertRect(keyboardFrame!, fromView: nil)
        let curveAnimationOption = UIViewAnimationOptions(rawValue: UInt(animationCurve))
        let options = UIViewAnimationOptions.BeginFromCurrentState
        UIView.animateWithDuration(animationDuration, delay: 0, options:options, animations: { () -> Void in
            var heightInternalSV = UIScreen.mainScreen().bounds.size.height
            if let _  = self.navigationController?.navigationBar{
                heightInternalSV -= 64.0
            }
            
            let insetHeight = (heightInternalSV + self.internalScrollView.frame.origin.y) - keyboardFrameConvertedToViewFrame.origin.y + 24
            self.internalScrollView.contentInset = UIEdgeInsetsMake(0, 0, insetHeight, 0)
            self.internalScrollView.scrollIndicatorInsets  = UIEdgeInsetsMake(0, 0, insetHeight, 0)
            }) { (complete) -> Void in
        }
    }
    func keyboardHasBarButtonWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo as! Dictionary<String, AnyObject>
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey]!.intValue
        let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue
        let keyboardFrameConvertedToViewFrame = view.convertRect(keyboardFrame!, fromView: nil)
        let curveAnimationOption = UIViewAnimationOptions(rawValue: UInt(animationCurve))
        let options = UIViewAnimationOptions.BeginFromCurrentState
        UIView.animateWithDuration(animationDuration, delay: 0, options:options, animations: { () -> Void in
            var heightInternalSV = UIScreen.mainScreen().bounds.size.height
            if let _  = self.navigationController?.navigationBar{
                heightInternalSV -= 64.0
            }
            
            let insetHeight = (heightInternalSV + self.internalScrollView.frame.origin.y) - keyboardFrameConvertedToViewFrame.origin.y + 32
            self.internalScrollView.contentInset = UIEdgeInsetsMake(0, 0, insetHeight, 0)
            self.internalScrollView.scrollIndicatorInsets  = UIEdgeInsetsMake(0, 0, insetHeight, 0)
            }) { (complete) -> Void in
        }
    }
    func keyboardHasErrorLabelAndBarButtonWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo as! Dictionary<String, AnyObject>
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey]!.intValue
        let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue
        let keyboardFrameConvertedToViewFrame = view.convertRect(keyboardFrame!, fromView: nil)
        let curveAnimationOption = UIViewAnimationOptions(rawValue: UInt(animationCurve))
        let options = UIViewAnimationOptions.BeginFromCurrentState
        UIView.animateWithDuration(animationDuration, delay: 0, options:options, animations: { () -> Void in
            var heightInternalSV = UIScreen.mainScreen().bounds.size.height
            if let _  = self.navigationController?.navigationBar{
                heightInternalSV -= 64.0
            }
            
            let insetHeight = (heightInternalSV + self.internalScrollView.frame.origin.y) - keyboardFrameConvertedToViewFrame.origin.y + 48
            self.internalScrollView.contentInset = UIEdgeInsetsMake(0, 0, insetHeight, 0)
            self.internalScrollView.scrollIndicatorInsets  = UIEdgeInsetsMake(0, 0, insetHeight, 0)
            }) { (complete) -> Void in
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let userInfo = notification.userInfo as! Dictionary<String, AnyObject>
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey]!.intValue
        let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue
        let keyboardFrameConvertedToViewFrame = view.convertRect(keyboardFrame!, fromView: nil)
        let curveAnimationOption = UIViewAnimationOptions(rawValue: UInt(animationCurve))
        let options = UIViewAnimationOptions.BeginFromCurrentState
        UIView.animateWithDuration(animationDuration, delay: 0, options:options, animations: { () -> Void in
            self.internalScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            self.internalScrollView.scrollIndicatorInsets  = UIEdgeInsetsMake(0, 0, 0, 0)
            }) { (complete) -> Void in
        }
    }
}
  