//
//  PDFViewController.swift
//  ViParking
//
//  Created by Ferico Samuel on 8/6/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class PDFViewController : UIViewController, UIWebViewDelegate
{
    @IBOutlet weak var webView: UIWebView!
    
    var vehicleID:String?
    var startDate:String?
    var endDate:String?
    
    override func viewDidLoad() {
        self.webView.delegate = self
        var urlString = Util.BASE_URL + "html/" + vehicleID!
        if let startDate = self.startDate, endDate = self.endDate {
            urlString += "/" + startDate + "/" + endDate
        }

        urlString = urlString.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        urlString = urlString.stringByReplacingOccurrencesOfString("API/", withString: "")
        if let url = NSURL(string: urlString) {
            let urlRequest = NSURLRequest(URL: url)
            self.webView.loadRequest(urlRequest)
        }
    }
    
    @IBAction func btnDoneClick(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let url = request.URL?.absoluteString {
            if !url.containsString(Util.BASE_URL.stringByReplacingOccurrencesOfString("API/", withString: "")) {
                return false
            }
            return true
        }
        else {
            return false
        }
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        showLoadingPopUp()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        dismissPopUp()
    }
}