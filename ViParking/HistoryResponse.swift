//
//  HistoryResponse.swift
//  ViParking
//
//  Created by Ferico Samuel on 8/6/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

class HistoryResponse : WSResponse {
    var histories:[History] = []
    
    override func parse(data: NSData) {
        super.parse(data)
        
        if let historyObjects = jsonObject["data"] as? [[String:AnyObject]] {
            for history in historyObjects {
                let h = History(jsonObject: history)
                histories.append(h)
            }
        }
    }
}