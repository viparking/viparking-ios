//
//  History.swift
//  ViParking
//
//  Created by Ferico Samuel on 8/6/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

class History {
    
    var id:String?
    var parkingLotID:Int?
    var vehicleID:String?
    var paymentTypeID:Int?
    var memberID:String?
    var price:Int?
    var inTime:String?
    var outTime:String?
    var inGate:String?
    var outGate:String?
    var parkingLotName:String?
    var startingBalance:Int?
    var balance:Int?
    var createdAt:String?
    
    init(jsonObject:[String:AnyObject]) {
        id = jsonObject["id"] as? String
        parkingLotID = jsonObject["parking_lot_id"] as? Int
        vehicleID = jsonObject["vehicle_id"] as? String
        paymentTypeID = jsonObject["payment_type_id"] as? Int
        memberID = jsonObject["member_id"] as? String
        price = jsonObject["price"] as? Int
        inTime = jsonObject["inTime"] as? String
        outTime = jsonObject["outTime"] as? String
        inGate = jsonObject["inGate"] as? String
        outGate = jsonObject["outGate"] as? String
        createdAt = jsonObject["created_at"] as? String
        parkingLotName = jsonObject["parkingLotName"] as? String
        startingBalance = jsonObject["startingBalance"] as? Int
        balance = jsonObject["balance"] as? Int
    }
}