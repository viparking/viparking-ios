//
//  Vehicle.swift
//  ViParking
//
//  Created by Ferico Samuel on 8/3/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

class Vehicle : NSObject, NSCoding {
    var id:String?
    var vehicleTypeID:Int?
    var memberID:String?
    var cardID:String?
    var name:String?
    var plateNumber:String?
    var balance:Int?
    var address:String?
    var noSTNK:String?
    var brand:String?
    var color:String?
    var type:String?
    
    var descriptions:String?{
        get{
            if let color = color, type = type, brand = brand {
                return color+" "+type+" "+brand
            }
            return nil
        }
    }
    
    init(jsonObject:[String:AnyObject]){
        id = jsonObject["id"] as? String
        vehicleTypeID = jsonObject["vehicle_type_id"] as? Int
        memberID = jsonObject["member_id"] as? String
        cardID = jsonObject["card_id"] as? String
        name = jsonObject["name"] as? String
        plateNumber = jsonObject["plateNumber"] as? String
        balance = jsonObject["balance"] as? Int
        address = jsonObject["address"] as? String
        noSTNK = jsonObject["noSTNK"] as? String
        brand = jsonObject["brand"] as? String
        color = jsonObject["color"] as? String
        type = jsonObject["type"] as? String
    }
    
    @objc func encodeWithCoder(aCoder: NSCoder){
        aCoder.encodeObject(id, forKey:"id")
        aCoder.encodeObject(vehicleTypeID, forKey:"vehicleTypeID")
        aCoder.encodeObject(memberID, forKey:"memberID")
        aCoder.encodeObject(cardID, forKey:"cardID")
        aCoder.encodeObject(name, forKey:"name")
        aCoder.encodeObject(plateNumber, forKey:"plateNumber")
        aCoder.encodeObject(balance, forKey:"balance")
        aCoder.encodeObject(address, forKey:"address")
        aCoder.encodeObject(noSTNK, forKey:"noSTNK")
        aCoder.encodeObject(brand, forKey:"brand")
        aCoder.encodeObject(color, forKey:"color")
        aCoder.encodeObject(type, forKey:"type")
    }
    
    @objc required init?(coder aDecoder: NSCoder){
        id = aDecoder.decodeObjectForKey("id") as? String
        vehicleTypeID = aDecoder.decodeObjectForKey("vehicleTypeID") as? Int
        memberID = aDecoder.decodeObjectForKey("memberID") as? String
        cardID = aDecoder.decodeObjectForKey("cardID") as? String
        name = aDecoder.decodeObjectForKey("name") as? String
        plateNumber = aDecoder.decodeObjectForKey("plateNumber") as? String
        balance = aDecoder.decodeObjectForKey("balance") as? Int
        address = aDecoder.decodeObjectForKey("address") as? String
        noSTNK = aDecoder.decodeObjectForKey("noSTNK") as? String
        brand = aDecoder.decodeObjectForKey("brand") as? String
        color = aDecoder.decodeObjectForKey("color") as? String
        type = aDecoder.decodeObjectForKey("type") as? String
    }
}