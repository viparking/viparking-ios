//
//  HTTPRequest.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/5/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import Foundation

public class HTTPRequest{
    internal var delegate:WSDelegate
    internal var request:NSMutableURLRequest
    internal var isUsingHeader:Bool?
    internal var responseObject:WSResponse?
    
    /// Initialize Web service helper class
    /// - parameters:
    ///     - GoodDealWSDelegate: class that implement this delegate which will be called on success or error during request
    ///     - NSURL: url where the request will be sent
    init(delegate:WSDelegate, url:NSURL, responseObject:WSResponse){
        self.delegate = delegate
        self.request = NSMutableURLRequest(URL: url)
        self.responseObject = responseObject
    }
    
    /// Send the request to the desired url within pre-set data
    internal func makeRequest(isUsingHeader: Bool){
        self.isUsingHeader = isUsingHeader
        let session = NSURLSession.sharedSession();
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if isUsingHeader
        {
            if let bearer = Util.generateBearerToken()
            {
                request.addValue("Bearer "+bearer, forHTTPHeaderField: "Authorization")
            }
        }
        if !Reachability.isConnectedToNetwork() {
            return self.delegate.onNoNetwork(self)
        }
        let task = session.dataTaskWithRequest(request)
            { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
                if let response = response as? NSHTTPURLResponse
                {
                    print(response.statusCode)
                    if response.statusCode >= 200 && response.statusCode <= 400
                    {
                        if let data = data
                        {
                            //if Util.IS_DEBUGGING{
                                print(NSString(data: data, encoding: NSUTF8StringEncoding))
                            //}
                            self.responseObject?.parse(data)
                            if !self.responseObject!.isSuccess {
                                self.delegate.onError((self.responseObject?.errorMessage)!, request: self)
                                return
                            }
                            self.delegate.onSuccess(self.responseObject!)
                        }
                    }
                    else if response.statusCode == 401
                    {
                        //logout
                        print(NSString(data: data!, encoding: NSUTF8StringEncoding))
//                        let err:NSError = NSError(domain: "Odeo", code: Util.NOT_LOGGED_IN, userInfo: nil)
                        self.delegate.onError("", request: self)
                        NSNotificationCenter.defaultCenter().postNotificationName(Util.SHOULD_LOGOUT, object: nil)
                        return
                    }
                    else
                    {
                        print(error?.description)
                        if error?.description == nil {
                            self.delegate.onError("", request: self)
                        }
                        else {
                            self.delegate.onError("", request: self)
                        }
                    }
                }
                else
                {
                    NSNotificationCenter.defaultCenter().postNotificationName(Util.SHOULD_LOGOUT, object: nil)
//                    let err:NSError = NSError(domain: "Odeo", code: Util.NOT_LOGGED_IN, userInfo: nil)
                    self.delegate.onError("", request: self)
                    return
                }
        }
        task.resume()
    }
    
    /// Retry sending the request automatically
    public func retry(){
        //if Util.IS_DEBUGGING{
            print(request.URL?.absoluteString)
            if let data = request.HTTPBody{
                print(NSString(data: data, encoding: NSUTF8StringEncoding))
            }
        //}
        makeRequest(self.isUsingHeader!)
    }
}
