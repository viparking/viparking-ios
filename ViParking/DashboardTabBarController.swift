//
//  DashboardTabBarController.swift
//  ViParking
//
//  Created by Ferico Samuel on 8/4/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class DashboardTabBarController : UITabBarController {
    
    static var instance:DashboardTabBarController?
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        DashboardTabBarController.instance = self
    }
}