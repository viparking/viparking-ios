//
//  VehicleDetailViewController.swift
//  ViParking
//
//  Created by Ferico Samuel on 7/7/16.
//  Copyright © 2016 ViParking. All rights reserved.
//

import UIKit

class VehicleDetailViewController : UIViewController, WSDelegate{
    
    @IBOutlet weak var imgVehicleIcon: UIImageView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var lblPlateNumber: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var btnHistory: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.VEHICLE_LIST = nil
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        circleView.layer.cornerRadius = 110
        btnHistory.layer.borderColor = UIColor(hexaString: "#FFFFFF", alphaValue: 0.3).CGColor
        btnHistory.layer.borderWidth = 1
        
        if let vehicleList = Util.VEHICLE_LIST {
            if vehicleList.count > 0 {
                if let selectedIndex = Util.SELECTED_VEHICLE_INDEX {
                    setVehicle(vehicleList[selectedIndex])
                }
                else {
                    setVehicle(vehicleList[0])
                }
            }
        }
        else {
            let ws = ParkingWS(delegate: self)
            showLoadingPopUp()
            ws.getVehicleList()
        }
    }
    
    @IBAction func btnHistoryClick(sender: AnyObject) {
        UIView.transitionFromView((DashboardTabBarController.instance?.selectedViewController!.view)!, toView: (DashboardTabBarController.instance?.viewControllers![2].view)!, duration: 0.5, options: UIViewAnimationOptions.TransitionFlipFromRight, completion: {
            (finished:Bool) -> () in
            DashboardTabBarController.instance?.selectedIndex = 2
        })
    }
    
    func setVehicle(vehicle:Vehicle){
        lblPlateNumber.text = vehicle.plateNumber
        if let balance = vehicle.balance {
            lblBalance.text = Util.makeMoneyFormat(balance)
        }
    }
    
    func onSuccess(responseObject:WSResponse){
        dismissPopUp({
            if let response = responseObject as? VehicleListResponse {
                Util.VEHICLE_LIST = response.vehicles
                self.setVehicle(response.vehicles[0])
            }
        })
    }
    
    func onError(error:String, request: HTTPRequest)
    {
        if error == "" {
            dismissPopUp({
                self.dismissViewControllerAnimated(true, completion: nil)
                Util.USER_EMAIL = ""
                Util.USER_ID = ""
                Util.ACCESS_TOKEN = ""
            })
            return
        }
        showErrorPopUp(error)
    }
    
    func onNoNetwork(request: HTTPRequest)
    {
        showRetryLoading(request)
    }
}